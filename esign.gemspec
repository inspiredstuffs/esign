lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "esign/version"

Gem::Specification.new do |spec|
  spec.name          = "esign"
  spec.version       = Esign::VERSION
  spec.authors       = ["Samuel I. Olugbemi"]
  spec.email         = ["luvolugbemmy@gmail.com"]

  spec.summary       = %q{Effortlessly integrate E-Signature solution with your Ruby on Rails App also with https://signfast.co.uk api service for digitally signing your documents}
  spec.description   = %q{Esign is a Ruby Gem that integrates E-Signature with your Ruby on Rails App effortlessly.It allows your ActiveRecord model to acts_as_signee (Users, Actors etc..) and also acts_as_signable (e.g. your Documents etc..) }
  spec.homepage      = "https://luvolugbemmy@bitbucket.org/inspiredstuffs/esign.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "activerecord"
  spec.add_development_dependency "appraisal"
  spec.add_development_dependency "logger"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rspec-mocks"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "yard"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "mock_redis"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"  
  spec.add_dependency "railties", ">= 4.1.0", "< 5.2"
  spec.add_dependency "bundler", "~> 1.16"
  spec.add_dependency "rake", ">= 10.0"
  spec.add_dependency 'bootstrap-sass', '~> 3.3.7'        
  spec.add_dependency 'sass-rails'
  spec.add_dependency 'jquery-rails'
  spec.add_dependency "simple_form", "4.0.0"
  spec.add_dependency "font-awesome-rails"
  spec.add_dependency "pdfkit"
  spec.add_dependency "rest-client"
end
