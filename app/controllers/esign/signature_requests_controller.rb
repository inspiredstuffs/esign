module Esign
  class SignatureRequestsController < ::ApplicationController#ActionController::Base
    include Rails.application.routes.url_helpers
    before_action :find_signature_request, only: [:index]
    # GET /resource/signature_requests?token=abcdef
    def index
      if !@signature_request.closed_at.blank?
        redirect_to root_path, alert: "Ooops! Signature request has been closed!"
      end
    end
    # PUT /resource/signature_requests/sign/:id
    def update
      # @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by_id(params[:id])
      # respond_to do |format|
      #   begin
      #     if @signature_request.signee.sign!(@signature_request.envelope.signable, permited_params)
      #       format.html {redirect_to root_path, notice: "Signature request completed. Thank You"}
      #       format.json {render json: {success: true, message: "Signature request completed. Thank You", url: root_path}}
      #     else
      #       format.html { redirect_to "/esign/signature_requests/#{@signature_request.token}", alert: "An error occurred. Please try again."}
      #       format.json {render json: {success: false, errors: ["An error occurred. Please try again."], url: "/esign/signature_requests/#{@signature_request.token}"}}
      #     end
      #   rescue Exception => e
      #     Rails.logger.error { e.backtrace }
      #     format.html { redirect_to "/esign/signature_requests/#{@signature_request.token}", alert: e.message }
      #     format.json {render json: {success: false, errors: [e.message], url: "/esign/signature_requests/#{@signature_request.token}"}}
      #   end
      # end      
    end

    def finalise_signing      
      @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by_id(params[:id])
      @signature_request.assign_attributes(permited_params)
      @signature_request.declaration_accepted = true
      @signature_request.status = "signed"
      # byebug
      @signature_request.closed_at = Time.now
      @envelope = @signature_request.envelope
      respond_to do |format|
        @success =  !params[:page_html].blank? && @envelope.update_attribute('content', params[:page_html].to_s) && @signature_request.save
        format.js        
      end      
    end

    def adopt_signature_or_initials
      @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by_id(params[:id])
      @envelope = @signature_request.envelope
      html = Nokogiri::HTML(@envelope.content)
      html.at_css("a[href='#{params[:href]}']").replace(params[:data])
      respond_to do |format|
        if @envelope.update_attribute('content', html.to_s)
          @signature_request.reload
          @hide = true
          @success = true
        else
          @success = false
        end
        format.js
      end
    end

    def decline_request
      @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by_id(params[:id])
      @signature_request.assign_attributes(permited_params)
      @signature_request.declaration_accepted = false
      @signature_request.status = "declined"
      @signature_request.closed_at = Time.now
      respond_to do |format|
        @success =  @signature_request.save!
        format.js        
      end       
    end 

    # ActionController::Base.helpers.sanitize()
    def download_copy
      @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by_id(params[:id])
      kit = PDFKit.new(@signature_request.envelope.content.to_s.encode('utf-8', 'binary', invalid: :replace, undef: :replace, replace: '').scrub(""),
      :header_center => "Page [page] from [toPage] pages",
      :header_spacing => '5',
      :header_line => true,
      :header_font_name => "Courier new",
      :header_font_size => "14",
      # :footer_center => initial_here_tabs_str,
      :footer_spacing => '5',
      :margin_bottom => '2cm',
      :margin_top    => '2cm',
      :margin_left   => '2cm',
      :margin_right  => '2cm')
      kit.stylesheets << "#{Esign::Engine.root}/app/assets/stylesheets/print_pdf.css"
      # Get an inline PDF
      send_data(kit.to_pdf, 
         :filename    => "Doc-#{@signature_request.token.to_s[0..11].to_s.upcase}.pdf", 
         :disposition => 'attachment')
    end

    def default_url_options
      { locale: I18n.locale }
    end
    
    protected
    def find_signature_request
      @signature_request = Esign::ActiveRecordStores::SignatureRequest.find_by(token: params[:token]) if params[:token]
      if @signature_request.blank?
        redirect_to root_path, alert: "Invalid signature request token!"
      end
    end

    def permited_params
      _params = params.require("signature_request").permit!.to_h.with_indifferent_access
      _params["signee_ip"]         = request.remote_ip
      _params["signee_user_agent"] = request.user_agent
      return _params
    end
  end
end