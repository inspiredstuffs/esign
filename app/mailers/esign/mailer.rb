# frozen_string_literal: true

if defined?(ActionMailer)
  class Esign::Mailer < Esign.parent_mailer.constantize
    include Esign::Mailers::Helpers

    def request_signature(record, opts={})    
      esign_mail(record, :request_signature, opts)
    end

    def signature_request_reminder(record, opts={})
      esign_mail(record, :signature_request_reminder, opts)
    end

    def signature_request_no_response(record, opts={})
      esign_mail(record, :signature_request_no_response, opts)
    end


    def signature_request_response(record, opts={})
      esign_mail(record, :signature_request_response, opts)  
    end

    def default_url_options
      {
        :host => Esign.host_url,
        :port => Esign.host_port
      }
    end
  end
end