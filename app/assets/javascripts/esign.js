//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery.signaturepad.js
//= require bootstrap-sprockets
//= require bootstrap.min.js
//= require bootbox.min.js
//= require interact.min.js



function resizeCanvas() {
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    signature_pad = new SignaturePad(canvas);
    signature_pad.clear(); // otherwise isEmpty() might return incorrect value
}

function getPosition(position){
  if($('.signature_pad_latitude'))
    $('.signature_pad_latitude').val(position.coords.latitude);
  if($('.signature_pad_longitude'))
    $('.signature_pad_longitude').val(position.coords.longitude);
}

function disabled_all_fields_except_ersd(){
  $("#signature_request_form :input").prop("disabled", true);
  $('.signature-ersd').prop("disabled", false);
}

function show_alert(size, title, message, callback){
    bootbox.alert(
      {
        size: size,
        title: title,
        message: message,
        callback: callback
      }
    )
}

draw_signature_and_initials = function(){
  generated_signature = document.getElementById("generated-signature-canvas");
  generated_signature_ctx = generated_signature.getContext("2d");
  generated_signature_ctx.clearRect(0, 0, generated_signature.width, generated_signature.height);
  generated_signature_ctx.beginPath();
  generated_signature_ctx.rect(30,30,30,0);
  generated_signature_ctx.rect(30,30,0,50);
  generated_signature_ctx.rect(30,80,30,0);

  generated_signature_ctx.strokeStyle = "black";
  generated_signature_ctx.stroke();

  generated_signature_ctx.font = "1.2rem fantasy";
  generated_signature_ctx.fillText("SignedFast By:",60,35);

  generated_signature_ctx.font = "2rem Pacifico";
  generated_signature_ctx.fillText($('#signature_request_signee_name').val(),40,65);

  generated_signature_ctx.font = "1.2rem fantasy";
  generated_signature_ctx.fillText($('#placeholder-klass').data('token'),60,85);

  generated_initials = document.getElementById("generated-initials-canvas");
  generated_initials_ctx = generated_initials.getContext("2d");
  generated_initials_ctx.clearRect(0, 0, generated_initials.width, generated_initials.height);
  generated_initials_ctx.beginPath();
  generated_initials_ctx.rect(30,30,20,0);
  generated_initials_ctx.rect(30,30,0,50);
  generated_initials_ctx.rect(30,80,70,0);

  generated_initials_ctx.strokeStyle = "black";
  generated_initials_ctx.stroke();

  generated_initials_ctx.font = "1.2rem fantasy";
  generated_initials_ctx.fillText("SignFast",50,35);

  generated_initials_ctx.font = "2rem Pacifico";
  generated_initials_ctx.fillText($('#signature_request_signee_initial').val().toUpperCase(),40,65);

}
// $(document).on('turbolinks:load', function() {

// });
bind_place_holder_caller = function(){
  $('.sr-placeholder').hide();
  var signee_klass = $('#placeholder-klass').data('klass'); 
  $('.'+signee_klass).show();
  $('#page_html').val($('#content-page > page').html());
  if($('.'+signee_klass)[0]){
    $('.'+signee_klass)[0].scrollIntoView();
  } else {
    $('.sign-declaration-finish').fadeIn(800);
    $('.sign-declaration-finish')[0].scrollIntoView();
      $('#signing-finish').unbind('click').click(function(event){
        event.preventDefault();
        $('#page_html').val($('#content-page > page').html());
        // $('form#signature_request_form').unbind().submit();
        toggle_loader();
        $.post(
          $(this).data('url'),
          $('form#signature_request_form').serialize(),
          function(data) {
            // if(data.success){
            //   toggle_loader();
            //   show_alert("large", "<h2>Success!<h2>", data.message,function(result){
            //     // $('#mask').show(); $('#progress').show();
            //     location.href = data.url
            //   });
            // }else{
            //   toggle_loader()
            //   show_alert("large", "Oooops!", "<div style='color:red'><ul>"+$.map(data.errors,function(a){ return '<li>'+a+'</li>' }).join('')+"</ul></div>",function(result){
            //     // if (data.url) {
            //     //   // $('#mask').show(); $('#progress').show();
            //     //   location.href = data.url;
            //     // }
            //   });
            // }
          }
        );        
      });     
  }
  $('.'+signee_klass).click(function(event){
    event.preventDefault();
    var href = $(this).attr('href')
    var placeholder_type = href.split('-')[1], id = href.split('-')[3], signature = $('#esign-signature').text(), initials = $('#esign-initials').text();
    // params = {href: href}
    if (placeholder_type == "signature") {
      $("a[href='"+href+"']").replaceWith(signature);
      // params.data = signature;
    } else if (placeholder_type == "initials") {
      $("a[href='"+href+"']").replaceWith(initials);
      // params.data = initials;
    }

    bind_place_holder_caller();
    resize_and_drag();    
    // $.post("/esign/signature_requests/"+id+"/adopt_signature_or_initials.js", params, function(data){
      
    // })    
  });  
}

  toggle_loader = function(){
    $(".loader").toggle();
    $(".loader").toggleClass('show');
    $(".main-container").toggleClass('backdrop');
  }

  resize_and_drag = function(){
    $('img.resizeable').resizable();    
    $.each($('div.draggable-div'), function( index, value ) {
      var rect = value.getBoundingClientRect();
      $(value).draggable({
        appendTo: 'body',
        containment: [ rect.x-50, rect.y-50, rect.x+100, rect.y+30 ],
        start: function(event, ui) {
            isDraggingMedia = true;
        },
        stop: function(event, ui) {
            isDraggingMedia = false;
            // blah
        }
      });  
    });    
    // interact('.resize-drag')
    //   .draggable({
    //     onmove: window.dragMoveListener,
    //     restrict: {
    //       restriction: 'parent',
    //       elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    //     },
    //   })
    //   .resizable({
    //     // resize from all edges and corners
    //     edges: { left: true, right: true, bottom: true, top: true },

    //     // keep the edges inside the parent
    //     restrictEdges: {
    //       outer: 'parent',
    //       endOnly: true,
    //     },

    //     // minimum size
    //     restrictSize: {
    //       min: { width: 100, height: 50 },
    //     },

    //     inertia: true,
    //   })
    //   .on('resizemove', function (event) {
    //     var target = event.target,
    //         x = (parseFloat(target.getAttribute('data-x')) || 0),
    //         y = (parseFloat(target.getAttribute('data-y')) || 0);

    //     // update the element's style
    //     target.style.width  = event.rect.width + 'px';
    //     target.style.height = event.rect.height + 'px';

    //     // translate when resizing from top or left edges
    //     x += event.deltaRect.left;
    //     y += event.deltaRect.top;

    //     target.style.webkitTransform = target.style.transform =
    //         'translate(' + x + 'px,' + y + 'px)';

    //     target.setAttribute('data-x', x);
    //     target.setAttribute('data-y', y);
    //     // target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height);
    //   });    
  }

// ctx is the 2d context of the canvas to be trimmed
// This function will return false if the canvas contains no or no non transparent pixels.
// Returns true if the canvas contains non transparent pixels
function trimCanvas(ctx) { // removes transparent edges
    var x, y, w, h, top, left, right, bottom, data, idx1, idx2, found, imgData;
    w = ctx.canvas.width;
    h = ctx.canvas.height;
    if (!w && !h) { return false } 
    imgData = ctx.getImageData(0, 0, w, h);
    data = new Uint32Array(imgData.data.buffer);
    idx1 = 0;
    idx2 = w * h - 1;
    found = false; 
    // search from top and bottom to find first rows containing a non transparent pixel.
    for (y = 0; y < h && !found; y += 1) {
        for (x = 0; x < w; x += 1) {
            if (data[idx1++] && !top) {  
                top = y + 1;
                if (bottom) { // top and bottom found then stop the search
                    found = true; 
                    break; 
                }
            }
            if (data[idx2--] && !bottom) { 
                bottom = h - y - 1; 
                if (top) { // top and bottom found then stop the search
                    found = true; 
                    break;
                }
            }
        }
        if (y > h - y && !top && !bottom) { return false } // image is completely blank so do nothing
    }
    top -= 1; // correct top 
    found = false;
    // search from left and right to find first column containing a non transparent pixel.
    for (x = 0; x < w && !found; x += 1) {
        idx1 = top * w + x;
        idx2 = top * w + (w - x - 1);
        for (y = top; y <= bottom; y += 1) {
            if (data[idx1] && !left) {  
                left = x + 1;
                if (right) { // if left and right found then stop the search
                    found = true; 
                    break;
                }
            }
            if (data[idx2] && !right) { 
                right = w - x - 1; 
                if (left) { // if left and right found then stop the search
                    found = true; 
                    break;
                }
            }
            idx1 += w;
            idx2 += w;
        }
    }
    left -= 1; // correct left
    if(w === right - left + 1 && h === bottom - top + 1) { return true } // no need to crop if no change in size
    w = right - left + 1;
    h = bottom - top + 1;
    ctx.canvas.width = w;
    ctx.canvas.height = h;
    ctx.putImageData(imgData, -left, -top);
    return true;            
}

$(function() {


  $('#page_html').val($('#content-page > page').html());
  $('.sr-placeholder').hide();
  $(".dropdown-toggle").dropdown();
  var signee_klass = $('#placeholder-klass').data('klass'); 
  if($('.'+signee_klass)[0]){
    //
  } else {
    $('.backdrop').hide();
    $('.modal-backdrop').remove();
    $('.sign-declaration').hide();
    $('.headers').hide();
    $('.navbar-info-bar').show();    
    $('.sign-declaration-finish').show();
    $('#signing-finish').click(function(event){
      event.preventDefault();
      // $('form#signature_request_form').unbind().submit();
      toggle_loader();
      $.post(
        $(this).data('url'),
        $('form#signature_request_form').serialize(),
        function(data) {
        }
      );        
    });     
    setTimeout(function(){
      $('.sign-declaration-finish')[0].scrollIntoView();
    }, 300)
    
  }

  $('#decline-request').unbind('click').click(function(event){
    event.preventDefault();
    var reason = prompt("Reason: ");
    if (reason){
      $('.signature_signee_comment').val(reason)
      toggle_loader();
      $.post(
        $(this).data('url'),
        $('form#signature_request_form').serialize(),
        function(data) {
        }
      );
    }
  });

  var vhtml = $("#adopt-sign").html();
  box_adopt = bootbox.dialog({
    message: vhtml,
    title: "",
    buttons: {
      cancel: {
        label: "Cancel",
        className: "btn-warning",
        callback: function () {
          box_adopt.modal('hide'); 
        }
      },
      success: {
        label: "Adopt Signature/Initials",
        className: "btn-success",
        callback: function (evt) {
          trimCanvas(generated_initials_ctx)
          if($('.nav-tabs > li.toggle-menu.active').data('view') == "generate-signature"){            
            trimCanvas(generated_signature_ctx)
            var initial = "<img src='"+generated_initials.toDataURL()+"' class='resizeable' style='width:64px;height:34px;'/>";
            var signature = "<img src='"+generated_signature.toDataURL()+"' class='resizeable' style='width:64px;height:34px;'/>";
          }else {
            trimCanvas(canvas.getContext("2d"))
            var initial = "<img src='"+generated_initials.toDataURL()+"' class='resizeable' style='width:64px;height:34px;'/>"; //$('#signature_request_signee_initial').val();
            var signature = "<img src='"+signature_pad.toDataURL()+"' class='resizeable' style='width:64px;height:34px;'/>";
          }
          
          
          if(signature && initial){
            $('#esign-initials').text(initial);
            $('#esign-signature').text(signature);
            $('#signature_request_declaration_accepted').val(true);
            $('#signature_request_signee_initial').val(initial);
            $('.backdrop').fadeOut(800);
            $('.modal-backdrop').remove();
            $('.sign-declaration').hide();
            $('.headers').hide();
            $('.navbar-info-bar').fadeIn(800);
            vhtml = $('.bootbox-body').html();            
            bind_place_holder_caller();
            resize_and_drag();
          } else {
          show_alert("large", "Oooops!", "<div style='color:red'><ul>Signature and initials are required to continue.</ul></div>",function(result){
    
                });            
            return false;
          }
        }
      }
    },
    show: false
  });

    box_adopt.on("hidden.bs.modal", function() {
      $("#adopt-sign").html(vhtml);      
    });

    box_adopt.on("shown.bs.modal", function() {
      $("#adopt-sign").html('');
      $('#generate-signature').show();
      var inital = (""+$('#signature_request_signee_name').val()).match(/\b(\w)/g).join(''); 
      $('#signature_request_signee_initial').val(inital);
      draw_signature_and_initials();
      $('.signature_pad_clear').click(function() { 
        signature_pad.clear() 
      });      
  
      // $("#signature_request_form :input").prop("disabled", false);      
        // $(".signature_pad").fadeIn(800);
      $('.nav-tabs > li.toggle-menu').click(function(){
        var view = $(this).data('view');
        if(!$(this).hasClass("active")){
          $('.nav-tabs > li.toggle-menu').removeClass('active')
          $(this).addClass('active');
          $('.signature_pad').hide();
          $('#'+view).show();
          if(view == "draw-signature"){
            canvas = document.getElementById("draw-signature-canvas"); 
            if (canvas){    
              window.addEventListener("resize", resizeCanvas);
              resizeCanvas();    
            }            
          }
        }
      });

     if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(getPosition);
      } else {
        console.log("Geolocation is not supported by this browser. Please try another browser to continue");
      }       
    });

      $('.signature_pad_clear').click(function() { 
        signature_pad.clear() 
      });  

  // $('#adopt-sign-btn').click(function(evt){
  //   evt.preventDefault();
  //   box_adopt.modal('show');    
   
  // })  
 
  // $('.generate-designs').change()
  $( '#declaration_accepted' ).click(function(evt) {
    var val = $(this).is(":checked");
    if (val) {
      // $('.content-container.backdrop').removeClass("backdrop");
      $('a#signing-continue').attr("disabled", false);
      // $(".signature_pad_clear").fadeIn(800);
      // $(".signature_attribs").fadeIn(800);

    } else {
      // $('.content-container').addClass("backdrop");
      $('a#signing-continue').attr("disabled", true);      
      // signature_pad.clear();
      // disabled_all_fields_except_ersd(); 
      // // $(".signature_attribs").fadeOut(1000);   
      // $(".signature_pad").fadeOut(1000); 
      // $(".signature_pad_clear").fadeOut(1000); 
    }
  });

  $('a#signing-continue').click(function(evt) {
    event.preventDefault();
    if ($( '#declaration_accepted' ).is(":checked")) {
      box_adopt.modal('show');
    }
  });

  resize_and_drag();
})