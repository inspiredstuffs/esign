# Esign

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/esign`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation
### Rails 3 and above
Add this line to your application's Gemfile:

```ruby
gem 'esign'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install esign

Run the generator:
`rails generate esign -s`

Or if you want to use Redis as your data store:
`rails generate esign -s --store=redis` (THIS IS COMING SOON! Still under development, but some core features works)

This will generate two (2) migration files (when using ActiveRecord) and 2 models named SignatureRequest and SignatureRequestLog. You may delete any of the SignatureRequest and SignatureRequestLog models and migrations if you don't need that functionality in your application.

Modify the Sprockets manifest in your application.js file to include:

    //= require esign

and also modify the application.css file to include:

    *= require esign

### Rails 2.3.x Support

This gem requires Rails 3 or better. Sorry!

## Usage

### Setup

Allows a model to be signatory (signee):

    class Tenant < ActiveRecord::Base
      ...
      acts_as_signee
      ...
    end

Allows a model to be signed (signee):

    class Document < ActiveRecord::Base
      ...
      acts_as_signable
      ...
    end

### Firstly, Create signature request envelope

To start requesting signature you need to create the signature request envelope using the signable model by passing the HTML content of the signable e.g.
  > NOTE: For compliance purpose, Users shouldn't be able to change the content of the signable once signature request has been sent out

    document.create_signature_envelope(signable_content)

To retrieve the signature envelope do:

    document.get_signature_envelope

### acts_as_signee Methods


check if a signee has been requested to sign a signable

    tenant.signature_requested_for?(document)

generate signature placeholder for signee signing a signable (These placeholders can be used in the html templates)

    tenant.signature_place_holder(document)

generate Initials placeholder for signee signing a signable (These placeholders can be used in the html templates)

    tenant.initials_place_holder(document)

check if a signee signed? a signable

    tenant.signed?(document)

What signable(s) has a signee been requested to sign (given that a Signable model is been signed)?

    tenant.signables(Document)

Number of sigantories (Requires signees_count column in db)

    def change
      add_column :#{Table_name}, :signables_count, :integer, :default => 0
    end

    document.signables_count

***

### acts_as_signable Methods

Request signature from a signee/signatory

    document.request_signature!(tenant, {signee_email: "example@demo.com", signee_name: "Test Demo"}) (without (!) returns the signature request record)

check if a signable has been requested to sign for signee

    document.signature_requested_from?(tenant)

get signature requested for a signable sent to a signee (Tenant)
    
    document.signature_request_record(tenant)

Send signature requests to signees

    document.send_signature_request_email(tenant, subject)

check if a signable is signed_by? a signee

    document.signed_by?(tenant)

returns signees(s) that has been requested to sign a signable

    document.signees(Tenant)

Number of sigantories (Requires signees_count column in db)

    def change
      add_column :#{Table_name}, :signees_count, :integer, :default => 0
    end

    document.signees_count

### acts_as_signable Methods for Digital signing via Signfast API

Digitally sign a acts_as_signable document by passing the reason and location as arguments 

    document.digitally_sign_document(reason, location)

Lookup a digitally signed document

    document.fetch_signed_document    

***
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/esign. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Esign project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/esign/blob/master/CODE_OF_CONDUCT.md).
