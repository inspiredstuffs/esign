Esign::Engine.routes.draw do
	# get 'signature_requests/:token' => 'signature_requests#index', as: :signature_requests
	# put 'signature_requests' => 'signature_requests#update', as: :update_signature_request
	resources :signature_requests do
	  collection do
	    get ':token' => 'signature_requests#index'
	  end

	  member do
	  	post :decline_request
	  	post :finalise_signing
	  	post :adopt_signature_or_initials
	  	get :download_copy
	  end
	end
end