require 'spec_helper'

describe Esign::ActiveRecordStores::SignatureRequest do
  before do
    @klass = Esign::ActiveRecordStores::SignatureRequest
    @klass.touch nil
    @klass.after_sign nil
    @klass.after_unsign nil
    @signee = ImASignee.create
    @signable = ImASignable.create
    @envelope = @signable.create_signature_envelope("<p>TESTING!!!</p>")
  end

  describe "data store" do
    it "inherits Esign::ActiveRecordStores::SignatureRequest" do
      expect(Esign.signature_request_model).to eq(Esign::ActiveRecordStores::SignatureRequest)
    end
  end

  describe "#signature_requested?" do
    it "returns true when SignatureRequest exists" do
      @klass.create! do |f|
        f.signee = @signee
        f.envelope = @envelope
      end
      expect(@klass.signature_requested_for?(@signee, @envelope)).to be true
    end

    it "returns false when SignatureRequest doesn't exist" do
      expect(@klass.signature_requested_for?(@signee, @envelope)).to be false
    end
  end


  describe "#signed?" do
    it "returns true when signable is signed" do
      @klass.create! do |f|
        f.signee = @signee
        f.envelope =  @envelope
        f.status = "signed"
      end
      expect(@klass.signed?(@signee, @envelope)).to be true
    end

    it "returns false when sign doesn't exist" do
      expect(@klass.signed?(@signee, @envelope)).to be false
    end
  end


end

