require 'spec_helper'

describe Esign::ActiveRecordStores::Mixins::Base do
  describe ".update_counter" do
    it "increments counter cache if column exists" do
      signable = ImASignableWithCounterCache.create

      update_counter(signable, signees_count: +1)

      expect(signable.reload.signees_count).to eq(1)
    end

    it "does not raise any errors if column doesn't exist" do
      signable = ImASignable.create
      update_counter(signable, signees_count: +1)
    end
  end

  def update_counter(model, counter)
    klass = Object.new
    klass.extend(Esign::ActiveRecordStores::Mixins::Base)
    klass.update_counter(model, counter)
  end
end

