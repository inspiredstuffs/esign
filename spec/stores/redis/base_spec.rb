require 'spec_helper'

describe Esign::RedisStores::Base do
  # Testing through RedisStores::SignatureRequest for easy testing
  # before(:each) do
  #   use_redis_store
  #   @klass = Esign::RedisStores::SignatureRequest
  #   @klass.touch nil
  #   @klass.after_sign nil
  #   @klass.after_unsign nil
  #   @signee1 = ImASignee.create
  #   @signee2 = ImASignee.create
  #   @signable1 = ImASignable.create
  #   @signable2 = ImASignable.create
  # end

  # describe "RedisStores::Base through RedisStores::SignatureRequest" do
  #   describe "Stores" do
  #     it "inherits Esign::RedisStores::SignatureRequest" do
  #       expect(Esign.signature_request_model).to eq(Esign::RedisStores::SignatureRequest)
  #     end
  #   end

  #   describe "#sign!" do
  #     it "creates sign records" do
  #       @klass.sign!(@signee1, @signable1)
  #       expect(Esign.redis.smembers(forward_key(@signable1))).to match_array ["#{@signee1.class}:#{@signee1.id}"]
  #       expect(Esign.redis.smembers(backward_key(@signee1))).to match_array ["#{@signable1.class}:#{@signable1.id}"]

  #       @klass.sign!(@signee2, @signable1)
  #       expect(Esign.redis.smembers(forward_key(@signable1))).to match_array ["#{@signee1.class}:#{@signee1.id}", "#{@signee2.class}:#{@signee2.id}"]
  #       expect(Esign.redis.smembers(backward_key(@signee1))).to match_array ["#{@signable1.class}:#{@signable1.id}"]
  #       expect(Esign.redis.smembers(backward_key(@signee2))).to match_array ["#{@signable1.class}:#{@signable1.id}"]
  #     end

  #     it "touches signee when instructed" do
  #       @klass.touch :signee
  #       expect(@signee1).to receive(:touch).once
  #       expect(@signable1).to receive(:touch).never
  #       @klass.sign!(@signee1, @signable1)
  #     end

  #     it "touches signable when instructed" do
  #       @klass.touch :signable
  #       expect(@signee1).to receive(:touch).never
  #       expect(@signable1).to receive(:touch).once
  #       @klass.sign!(@signee1, @signable1)
  #     end

  #     it "touches all when instructed" do
  #       @klass.touch :all
  #       expect(@signee1).to receive(:touch).once
  #       expect(@signable1).to receive(:touch).once
  #       @klass.sign!(@signee1, @signable1)
  #     end

  #     it "calls after sign hook" do
  #       @klass.after_sign :after_sign
  #       expect(@klass).to receive(:after_sign).once
  #       @klass.sign!(@signee1, @signable1)
  #     end

  #     it "calls after unsign hook" do
  #       @klass.after_sign :after_unsign
  #       expect(@klass).to receive(:after_unsign).once
  #       @klass.sign!(@signee1, @signable1)
  #     end
  #   end

  #   describe "#unsign!" do
  #     before(:each) do
  #       @klass.sign!(@signee1, @signable1)
  #     end

  #     it "removes sign records" do
  #       @klass.unsign!(@signee1, @signable1)
  #       expect(Esign.redis.smembers(forward_key(@signable1))).to be_empty
  #       expect(Esign.redis.smembers(backward_key(@signee1))).to be_empty
  #     end
  #   end

  #   describe "#signed?" do
  #     it "returns true when sign exists" do
  #       @klass.sign!(@signee1, @signable1)
  #       expect(@klass.signed?(@signee1, @signable1)).to be true
  #     end

  #     it "returns false when sign doesn't exist" do
  #       expect(@klass.signed?(@signee1, @signable1)).to be false
  #     end
  #   end

  #   describe "#signees" do
  #     it "returns an array of signees" do
  #       signee1 = ImASignee.create
  #       signee2 = ImASignee.create
  #       signee1.sign!(@signable1)
  #       signee2.sign!(@signable1)
  #       expect(@klass.signees(@signable1, signee1.class)).to match_array [signee1, signee2]
  #     end

  #     it "returns an array of signee ids when plucking" do
  #       signee1 = ImASignee.create
  #       signee2 = ImASignee.create
  #       signee1.sign!(@signable1)
  #       signee2.sign!(@signable1)
  #       expect(@klass.signees(@signable1, signee1.class, :pluck => :id)).to match_array ["#{signee1.id}", "#{signee2.id}"]
  #     end
  #   end

  #   describe "#signables" do
  #     it "returns an array of signables" do
  #       signable1 = ImASignable.create
  #       signable2 = ImASignable.create
  #       @signee1.sign!(signable1)
  #       @signee1.sign!(signable2)

  #       expect(@klass.signables(@signee1, signable1.class)).to match_array [signable1, signable2]
  #     end

  #     it "returns an array of signables ids when plucking" do
  #       signable1 = ImASignable.create
  #       signable2 = ImASignable.create
  #       @signee1.sign!(signable1)
  #       @signee1.sign!(signable2)
  #       expect(@klass.signables(@signee1, signable1.class, :pluck => :id)).to match_array ["#{signable1.id}", "#{signable2.id}"]
  #     end
  #   end

  #   describe "#generate_forward_key" do
  #     it "returns valid key when passed an object" do
  #       expect(forward_key(@signable1)).to eq("Signees:#{@signable1.class.name}:#{@signable1.id}")
  #     end

  #     it "returns valid key when passed a String" do
  #       expect(forward_key("Signable:1")).to eq("Signees:Signable:1")
  #     end
  #   end

  #   describe "#generate_backward_key" do
  #     it "returns valid key when passed an object" do
  #       expect(backward_key(@signee1)).to eq("Signables:#{@signee1.class.name}:#{@signee1.id}")
  #     end

  #     it "returns valid key when passed a String" do
  #       expect(backward_key("Signee:1")).to eq("Signables:Signee:1")
  #     end
  #   end

  #   describe "#remove_signees" do
  #     it "deletes all signees relationships for a signable" do
  #       @signee1.sign!(@signable1)
  #       @signee2.sign!(@signable1)
  #       expect(@signable1.signees(@signee1.class).count).to eq(2)

  #       @klass.remove_signees(@signable1)
  #       expect(@signable1.signees(@signee1.class).count).to eq(0)
  #       expect(Esign.redis.smembers(forward_key(@signable1))).to be_empty
  #       expect(Esign.redis.smembers(backward_key(@signee1))).to be_empty
  #       expect(Esign.redis.smembers(backward_key(@signee2))).to be_empty
  #     end
  #   end

  #   describe "#remove_signables" do
  #     it "deletes all signables relationships for a signee" do
  #       @signee1.sign!(@signable1)
  #       @signee1.sign!(@signable2)
  #       expect(@signee1.signables(@signable1.class).count).to eq(2)

  #       @klass.remove_signables(@signee1)
  #       expect(@signee1.signables(@signable1.class).count).to eq(0)
  #       expect(Esign.redis.smembers backward_key(@signable1)).to be_empty
  #       expect(Esign.redis.smembers backward_key(@signee2)).to be_empty
  #       expect(Esign.redis.smembers forward_key(@signee1)).to be_empty
  #     end
  #   end

  #   describe "#key_type_to_type_names" do
  #     it "returns the proper arrays" do
  #       expect(@klass.send(:key_type_to_type_names, Esign::RedisStores::SignatureRequest)).to eq(['signee', 'signable'])
  #     end
  #   end
  # end

  # # Helpers
  # def forward_key(signable)
  #   Esign::RedisStores::SignatureRequest.send(:generate_forward_key, signable)
  # end

  # def backward_key(signee)
  #   Esign::RedisStores::SignatureRequest.send(:generate_backward_key, signee)
  # end
end
