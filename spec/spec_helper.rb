$MOCK_REDIS = true
require "bundler/setup"
$:.push File.expand_path("../lib", __FILE__)
require 'active_record'
require 'active_support/all'
require "esign"
require 'spec_support/data_stores'
require 'spec_support/matchers'
require 'logger'
require "byebug"
require "sqlite3"
# require "paperclip/matchers"
Bundler.setup

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!
  # config.include Paperclip::Shoulda::Matchers
  config.expect_with :rspec do |c|
    c.syntax = :expect
    c.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups  
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
  config.expose_dsl_globally = true 
  Kernel.srand config.seed 
end



ActiveSupport::Inflector.inflections do |inflect|
	inflect.irregular 'cache', 'caches'
end