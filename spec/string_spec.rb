require 'spec_helper'

describe String do
  describe "#deep_const_get" do
    it "should return a class" do
      expect("Esign".deep_const_get).to eq(Esign)
      expect("Esign::ActiveRecordStores".deep_const_get).to eq(Esign::ActiveRecordStores)
      expect("Esign::ActiveRecordStores::SignatureRequest".deep_const_get).to eq(Esign::ActiveRecordStores::SignatureRequest)

      expect { "Foo::Bar".deep_const_get }.to raise_error(NameError)
    end
  end
end
