require 'mock_redis' if $MOCK_REDIS
require 'redis' unless $MOCK_REDIS

silence_warnings do
  Redis = MockRedis if $MOCK_REDIS # Magic!
end

def use_redis_store
  Esign.signature_request_model = Esign::RedisStores::SignatureRequest
  setup_model_shortcuts
end

def use_ar_store
  Esign.signature_request_model = Esign::ActiveRecordStores::SignatureRequest
  setup_model_shortcuts
end

def setup_model_shortcuts
  $Sign = Esign.signature_request_model
end

def clear_redis
  Esign.redis.keys(nil).each do |k|
    Esign.redis.del k
  end
end

ActiveRecord::Base.configurations = {'test' => {'adapter' => 'sqlite3', 'database' => ':memory:'}}
ActiveRecord::Base.establish_connection(:test)

ActiveRecord::Base.logger = Logger.new(STDERR)
ActiveRecord::Base.logger.level = Logger::WARN

ActiveRecord::Migration.verbose = false
ActiveRecord::Schema.define(:version => 0) do
  create_table :users do |t|
    t.string :name
  end

  create_table :tenants do |t|
    t.string :name
  end

  create_table :landlords do |t|
    t.string :name
  end    

  create_table :documents do |t|
    t.string      :name
    t.string      :doc_file_name
    t.string      :doc_content_type
    t.integer     :doc_file_size
    t.datetime    :doc_updated_at
    t.timestamps    
  end

    create_table    :esign_envelopes do |t|
      t.string      :ref, index: true, unique: true
      t.integer     :signable_id    
      t.string      :signable_type
      t.text        :content, limit: 16.megabytes - 1
      t.timestamps
    end

    add_index :esign_envelopes, ["signable_id", "signable_type"], :name => "fk_signables"

    create_table    :esign_signature_requests do |t|
      t.integer     :signee_id
      t.string      :signee_type
      t.integer     :envelope_id    
      t.string      :token, index: true, unique: true
      t.string      :status
      t.datetime    :sent_at
      t.datetime    :closed_at
      t.boolean     :declaration_accepted,  default: false
      t.string      :signee_name
      t.string      :signee_email
      t.text        :signee_comment
      t.string      :signee_ip
      t.string      :signee_user_agent      
      t.decimal     :signee_latitude, precision: 10, scale: 6
      t.decimal     :signee_longitude, precision: 10, scale: 6
      t.timestamps
    end

    add_index :esign_signature_requests, ["signee_id", "signee_type"],     :name => "fk_signees"

    create_table    :esign_signature_request_logs do |t|      
      t.integer     :signature_request_id, index: true
      t.string      :status
      t.string      :subject
      t.text        :body 
      t.timestamps
    end 

  create_table :im_a_signees do |t|
    t.timestamps null: true
  end

  # create_table :im_a_signee_with_counter_caches do |t|
  #   t.integer :signees_count, default: 0
  #   t.timestamps null: true
  # end

  create_table :im_a_signables do |t|
    t.timestamps null: true
  end

  create_table :im_a_signable_with_counter_caches do |t|
    t.integer :signees_count, default: 0
    t.timestamps null: true
  end


  create_table :vanillas do |t|
    t.timestamps null: true
  end
end

class ::Document < ActiveRecord::Base
  acts_as_signable  
end

class ::User < ActiveRecord::Base
  acts_as_signee
end



# class SignatureRequest < Esign::ActiveRecordStores::SignatureRequest; end

class ::ImASignee < ActiveRecord::Base
  acts_as_signee
end
class ::ImASigneeWithCounterCache < ActiveRecord::Base
  acts_as_signee
end
class ::ImASigneeChild < ImASignee; end

class ::ImASignable < ActiveRecord::Base
  acts_as_signable
end
class ::ImASignableWithCounterCache < ActiveRecord::Base
  acts_as_signable
end
class ::ImASignableChild < ImASignable; end

class ::Vanilla < ActiveRecord::Base
end

