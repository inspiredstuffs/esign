require 'spec_helper'

# Test Socialization as it would be used in a "real world" scenario
describe "The World" do
  cattr_reader :users, :docs

  %w(john jane mat carl camilo tami).each do |user|
    define_method(user) { @users[user.to_sym] }
  end

  %w(contract inventory deposit).each do |doc|
    define_method(doc) { @docs[doc.to_sym] }
  end

  before(:all) do
    seed
  end

  shared_examples "the world" do
    # it "acts like it should" do

    #   # john.sign!(contract)
    #   # john.sign!(inventory)


    #   # expect(john.signed?(contract)).to be true
    #   # expect(john.signed?(inventory)).to be true


    #   # expect(contract.signed_by?(john)).to be true


    #   # # Can't sign a movie
    #   # expect { john.sign!(kill_bill) }.to raise_error(Esign::ArgumentError)

    # end
    # it "acts like document should request signature from user" do
    #   contract.request_signature!(john, {signee_name: "Test Tester", signee_email: "test@example.com"})
    # end    
  end

  context "ActiveRecord store" do
    before(:all) { use_ar_store }
    it_behaves_like "the world"
  end

  context "Redis store" do
    before(:all) { use_redis_store }
    it_behaves_like "the world"
  end

  def seed
    @users    = {}
    @docs   = {}
    @movies   = {}

    @users[:john]       = User.create :name => 'John Doe'
    @users[:jane]       = User.create :name => 'Jane Doe'
    @users[:mat]        = User.create :name => 'Mat'
    @users[:carl]       = User.create :name => 'Carl'
    @users[:camilo]     = User.create :name => 'Camilo'
    @users[:tami]       = User.create :name => 'Tami'

    @docs[:contract]    = Document.create :name => 'Contract Document'
    @docs[:inventory]  = Document.create :name => 'Check-In Inventory'
    @docs[:deposit]   = Document.create :name => 'TDS Agreement'
  end

end
