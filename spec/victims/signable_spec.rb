require 'spec_helper'

describe Esign::Signable do
  before(:all) do
    use_ar_store
    @signee = ImASignee.new
    @signable = ImASignable.create
    @envelope = @signable.create_signature_envelope("<p>TESTING!!!</p>")
    @options = {signee_name: "Example Demo", signee_email: "example@demo.com"}
  end


  describe "#initialize envelope" do
    it "Creates signature Envelope for a signable" do
      expect(@envelope).eql?(@signable.get_signature_envelope)
    end
  end


  describe "#request_signature!" do
    it "can get signature from Signee and return value should be true" do
      expect(@signable.request_signature!(@signee, @options)).to be true
    end
  end

  describe "#request_signature" do
    it "can get signature from Signee and return value should be SignatureRequest instance" do
      expect(@signable.request_signature(@signee, @options))
      .eql?(Esign::ActiveRecordStores::SignatureRequest.signature_request_for(@signee, @signable))
    end
  end   


  describe "#is_signable?" do
    it "returns true" do
      expect(@signable.is_signable?).to be true
    end
  end 

  describe "#signable?" do
    it "returns true" do
      expect(@signable.signable?).to be true
    end
  end

  describe "#signed_by?" do
    it "does not accept non-signees" do
      expect { @signable.signed_by?(:foo) }.to raise_error(Esign::ArgumentError)
    end

    # it "calls $Sign.signed?" do
    #   expect($Sign).to receive(:signed?).with(@signee, @signable).once
    #   @signable.signed_by?(@signee)
    # end
  end

  describe "#signees" do
    # it "calls $Sign.signees" do
    #   expect($Sign).to receive(:signees).with(@signable, @signee.class, { :foo => :bar })
    #   @signable.signees(@signee.class, { :foo => :bar })
    # end
  end

  describe "#signees_relation" do
    # it "calls $Sign.signees_relation" do
    #   expect($Sign).to receive(:signees_relation).with(@signable, @signee.class, { :foo => :bar })
    #   @signable.signees_relation(@signee.class, { :foo => :bar })
    # end
  end

  # describe "deleting a signable" do
  #   before(:all) do
  #     @signee = ImASignee.create
  #     @signee.sign!(@signable)
  #   end

  #   it "removes sign relationships" do
  #     expect(Esign.signature_request_model).to receive(:remove_signees).with(@signable)
  #     @signable.destroy
  #   end
  # end
end

