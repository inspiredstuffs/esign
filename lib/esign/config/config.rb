module Esign
  class << self
    
    def signature_request_model
      if @signature_request_model
        @signature_request_model
      else
        Esign::ActiveRecordStores::SignatureRequest
      end
    end

    def signature_request_model=(klass)
      @signature_request_model = klass
    end

    def envelope_model
      if @envelope_model
        @envelope_model
      else
        Esign::ActiveRecordStores::Envelope
      end
    end

    def envelope_model=(klass)
      @envelope_model = klass
    end    

  end
end