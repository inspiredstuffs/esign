module ActiveRecord
  class Base
    def is_signable?
      false
    end
    alias signable? is_signable?
  end
end

module Esign
  module Signable
    extend ActiveSupport::Concern

    included do
      # after_destroy { Esign.signature_request_model.remove_signees(self) }

      # Specifies if self can be signed.
      #
      # @return [Boolean]
      def is_signable?
        true
      end
      alias signable? is_signable?

      # Request Signature from {signee} object.
      #
      # @return [Boolean]
      def request_signature!(signee, opts)
        opts = opts.with_indifferent_access
        raise Esign::ArgumentError, "Create signature envelope to continue!!"  if get_signature_envelope.blank?
        raise Esign::ArgumentError, "Missing required attributes!"  if opts.blank?         
        raise Esign::ArgumentError, "Missing signer's name (:signee_name)!"  if opts[:signee_name].blank?
        raise Esign::ArgumentError, "Missing signer's email (:signee_email)!"  if opts[:signee_email].blank?        
        raise Esign::ArgumentError, "#{signee} is not signee!"  unless signee.respond_to?(:is_signee?) && signee.is_signee?        
        Esign.signature_request_model.request_signature!(signee, get_signature_envelope, opts)
      end

      # Creates Signature requests evnvelope for {signable} .
      #
      # @return [Object]
      def create_signature_envelope(signable_content)
        Esign.envelope_model._initialize_(self, signable_content)
      end

      # Returns Signature requests evnvelope for {signable} .
      #
      # @return [Object]
      def get_signature_envelope
        Esign.envelope_model.get_envelope(self)
      end
      
      # Request Signature from {signee} object.
      #
      # @return [Object]
      def request_signature(signee, opts)
        opts = opts.with_indifferent_access
        raise Esign::ArgumentError, "Create signature envelope to continue! Refer to documentation"  if get_signature_envelope.blank?
        raise Esign::ArgumentError, "Missing required attributes!"  if opts.blank?
        raise Esign::ArgumentError, "Missing signer's name (:signee_name)!"  if opts[:signee_name].blank?
        raise Esign::ArgumentError, "Missing signer's email (:signee_email)!"  if opts[:signee_email].blank?
        raise Esign::ArgumentError, "#{signee} is not signee!"  unless signee.respond_to?(:is_signee?) && signee.is_signee?        
        Esign.signature_request_model.request_signature(signee, get_signature_envelope, opts)
      end

      # Specifies if self is signed by a {signee} object.
      #
      # @return [Boolean]
      def signed_by?(signee)
        raise Esign::ArgumentError, "#{signee} is not signee!"  unless signee.respond_to?(:is_signee?) && signee.is_signee?
        Esign.signature_request_model.signed?(signee, get_signature_envelope)
      end

      # Specifies if self has been asked to signed by {Signee} object.
      #
      # @param [Signable] signable the {Signable} object to test against.
      # @return [Boolean]
      def signature_requested_from?(signee)
        raise Esign::ArgumentError, "#{signee} is not signee!" unless signee.respond_to?(:is_signee?) && signee.signee?
        Esign.signature_request_model.signature_requested_for?(signee, get_signature_envelope)
      end        

      # Returns the SignatureRequest Object for the both {signable} & {signee}.
      #
      # @param [signee] signable the {Signable} object to test against.
      def signature_request_record(signee)
        raise Esign::ArgumentError, "#{signee} is not signee!" unless signee.respond_to?(:is_signee?) && signee.is_signee?
        Esign.signature_request_model.signature_request_for(signee, get_signature_envelope)       
      end

      def send_signature_request_email(signee, subject=nil)      
        raise Esign::ArgumentError, "#{signee} is not signee!" unless signee.respond_to?(:is_signee?) && signee.is_signee?
        Esign.signature_request_model.send_signature_request_email(signee, get_signature_envelope, subject)       
      end


      # Sends PDF data to signfast.co.uk to digitally sign document.
      #
      # @param [signee] signable the {Signable} object to test against.      
      def digitally_sign_document(reason, location)
        raise Esign::ArgumentError, "Your app needs to be registered on https://signfast.co.uk before you can digitally sign a document. \r\n Please provide your Signfast API key in config/initializers/esign.rb" if Esign.signfast_api_key.blank?
        raise Esign::ArgumentError, "Signature reason is required" if reason.blank?
        raise Esign::ArgumentError, "Signature location is required" if location.blank?
        get_signature_envelope.digitally_sign_document(reason, location)
      end

      def fetch_signed_document
        raise Esign::ArgumentError, "Your app needs to be registered on https://signfast.co.uk before you can digitally sign a document. \r\n Please provide your Signfast API key in config/initializers/esign.rb" if Esign.signfast_api_key.blank?
        get_signature_envelope.fetch_signed_document
      end

      # Returns an array of {signee}s signing self.
      #
      # @param [Class] klass the {signee} class to be included. e.g. `User`
      # @return [Array<signee, Numeric>] An array of signee objects or IDs
      def signees(klass, opts = {})
        opts = opts.with_indifferent_access
        Esign.signature_request_model.signees(get_signature_envelope, klass, opts)
      end

      # Returns a scope of the {signee}s signing self.
      #
      # @param [Class] klass the {signee} class to be included in the scope. e.g. `User`
      # @return ActiveRecord::Relation
      def signees_relation(klass, opts = {})
        opts = opts.with_indifferent_access
        Esign.signature_request_model.signees_relation(get_signature_envelope, klass, opts)
      end
    end
  end
end