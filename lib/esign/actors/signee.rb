module ActiveRecord
  class Base
    def is_signee?
      false
    end
    alias signee? is_signee?
  end
end

module Esign
  module Signee
    extend ActiveSupport::Concern

    included do
      # after_destroy { Esign.signature_request_model.remove_signables(self) }

      # Specifies if self can sign {Signable} objects.
      #
      # @return [Boolean]
      def is_signee?
        true
      end
      alias signee? is_signee?

      # Create a new {Sign sign} relationship.
      #
      # @param [Signable] signable the object to be signed.
      # @return [Boolean]
      def sign!(signable, opts={})
        opts = opts.with_indifferent_access
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        raise Esign::ArgumentError, "Signing params are missing!" if opts.blank?
        # raise Esign::ArgumentError, "Signee Email is required!" if opts[:signee_email].blank?
        # raise Esign::ArgumentError, "#{opts[:signee_email]} doesn't match signatory email" if Esign.signature_request_model.decrypt_e_tag(self, signable.get_signature_envelope) != opts[:signee_email].to_s
        raise Esign::ArgumentError, "No signature request record found for #{signable} and #{self}"  unless Esign.signature_request_model.signature_requested_for?(self, signable.get_signature_envelope)
        raise Esign::ArgumentError, "Missing required signature attributes!" if opts[:signature_request].blank?
        raise Esign::ArgumentError, "Signatory IP address is required!" if opts[:signature_request][:signee_ip].blank?
        raise Esign::ArgumentError, "Signatory browsing Agent is required is required!" if opts[:signature_request][:signee_user_agent].blank?
        raise Esign::ArgumentError, "Accept or Decline declaration to proceed!"  if opts[:signature_request][:declaration_accepted].blank?
        # raise Esign::ArgumentError, "Signature is required if declaration has been accepted." if opts[:signature_request][:declaration_accepted].to_s == "true" && opts[:signature].blank?
        raise Esign::ArgumentError, "Comment cannot be blank if declaration is declined." if opts[:signature_request][:declaration_accepted].to_s == "false" && opts[:signature_request][:signee_comment].blank?        
        Esign.signature_request_model.sign!(self, signable.get_signature_envelope, opts)
      end

      # Delete a {Sign sign} relationship.
      #
      # @param [Signable] signable the object to unsign.
      # @return [Boolean]
      def unsign!(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        Esign.signature_request_model.unsign!(self, signable.get_signature_envelope)
      end


      # Toggles a {Sign sign} relationship.
      #
      # @param [Signable] signable the object to sign/unsign.
      # @return [Boolean]
      def toggle_sign!(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        if signed?(signable)
          unsign!(signable)
          false
        else
          sign!(signable)
          true
        end
      end

      # Returns the SignatureRequest Object for the both {signable} & {signee}.
      #
      # @param [Signable] signable the {Signable} object to test against.
      def signature_request_record(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        Esign.signature_request_model.signature_request_for(self, signable.get_signature_envelope)        
      end

      # Returns the SignatureRequest Object for the both {signable} & {signee}.
      #
      # @param [Signable] signable the {Signable} object to test against.
      def decrypt_e_tag(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        Esign.signature_request_model.decrypt_e_tag(self, signable.get_signature_envelope)        
      end 

      # Specifies if self signed a {Signable} object.
      #
      # @param [Signable] signable the {Signable} object to test against.
      # @return [Boolean]
      def signed?(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        Esign.signature_request_model.signed?(self, signable.get_signature_envelope)
      end

      def signature_place_holder(signable)
          raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
          signature_request = signature_request_record(signable)
          return "<em class='text-danger'><strong>No signature request sent to this signatory yet!</strong></em>" if signature_request.blank?
          signature_id = "signature-#{SecureRandom.hex(3)}-#{signature_request.id}"
          return "<div class='draggable-div' id='div-#{signature_id}'><a href='#-#{signature_id}' data-placeholdertype='signature' class='sr-placeholder #{self.class.name.underscore}-#{self.id} btn btn-warning'> <strong>SIGN</strong> <br/> <i class='fa fa-caret-square-o-down fa-2x'></i></a></div>"      #data-srid='#{signature_request.id}' id='#{SecureRandom.hex(3)}'
      end

      def initials_place_holder(signable)
          raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
          signature_request = signature_request_record(signable)
          return "<em class='text-danger'><strong>No signature request sent to this signatory yet!</strong></em>" if signature_request.blank?
          initials_id = "initials-#{SecureRandom.hex(3)}-#{signature_request.id}"
          return "<div class='draggable-div' id='div-#{initials_id}'><a href='#-#{initials_id}' data-placeholdertype='initials' class='sr-placeholder #{self.class.name.underscore}-#{self.id} btn btn-warning'> <strong>INITIAL</strong> <br/> <i class='fa fa-caret-square-o-down fa-2x'></i></a></div>"      #data-srid='#{signature_request.id}' id='#{SecureRandom.hex(3)}' 
      end      
      # Specifies if self has been asked to sign a {Signable} object. 
      #style='display:none;'
      # @param [Signable] signable the {Signable} object to test against.
      # @return [Boolean]
      def signature_requested_for?(signable)
        raise Esign::ArgumentError, "#{signable} is not signable!" unless signable.respond_to?(:is_signable?) && signable.is_signable?
        Esign.signature_request_model.signature_requested_for?(self, signable.get_signature_envelope)
      end

      # Returns all the signables of a certain type that are signed by self
      #
      # @params [Signable] klass the type of {Signable} you want
      # @params [Hash] opts a hash of options
      # @return [Array<Signable, Numeric>] An array of Signable objects or IDs
      def signables(klass, opts = {})
        opts = opts.with_indifferent_access
        Esign.signature_request_model.signables(self, klass, opts)
      end
      alias :signablers :signables

      # Returns a relation for all the signables of a certain type that are signed by self
      #
      # @params [Signable] klass the type of {Signable} you want
      # @params [Hash] opts a hash of options
      # @return ActiveRecord::Relation
      def signables_relation(klass, opts = {})
        opts = opts.with_indifferent_access
        Esign.signature_request_model.signables_relation(self, klass, opts)
      end
      alias :signablers_relation :signables_relation
    end
  end
end