require 'active_support/concern'

module Esign
  module ActsAsHelpers
    extend ActiveSupport::Concern

    module ClassMethods
      # Make the current class a {Esign::Signee}
      def acts_as_signee(opts = {})
        include Esign::Signee
      end

      # Make the current class a {Esign::Signable}
      def acts_as_signable(opts = {})
        include Esign::Signable
      end

    end
  end
end
