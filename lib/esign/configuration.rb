module Esign
  class Configuration
    attr_accessor :close_after, :deadline_reminder

    def initialize      
    	@close_after = 5
    	@deadline_reminder = 1
    end
  end
end