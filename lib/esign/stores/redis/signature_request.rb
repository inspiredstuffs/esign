module Esign
  module RedisStores
    class SignatureRequest < Esign::RedisStores::Base
      extend Esign::Stores::Mixins::Base
      extend Esign::Stores::Mixins::SignatureRequest
      extend Esign::RedisStores::Mixins::Base

      class << self
        alias_method :request_signature!, :relation!;             public :request_signature!
        alias_method :signature_request_record!, :relation!;      public :signature_request_record!
        alias_method :sign!, :relation!;                          public :sign!        
        alias_method :unsign!, :unrelation!;                      public :unsign!
        alias_method :signs?, :relation?;                         public :signs?
        alias_method :signees_relation, :actors_relation;         public :signees_relation
        alias_method :signees, :actors;                           public :signees
        alias_method :signables_relation, :victims_relation;      public :signables_relation
        alias_method :signables, :victims;                        public :signables
        alias_method :remove_signees, :remove_actor_relations;    public :remove_signees
        alias_method :remove_signables, :remove_victim_relations; public :remove_signables
      end

    end
  end
end
