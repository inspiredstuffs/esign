module Esign
  module ActiveRecordStores
    class SignatureRequest < ActiveRecord::Base
      extend Esign::Stores::Mixins::Base
      extend Esign::Stores::Mixins::SignatureRequest
      extend Esign::ActiveRecordStores::Mixins::Base
      # extend Esign::Mailers::Helpers
      belongs_to :envelope
      belongs_to :signee,   :polymorphic => true
      has_many :signature_request_logs   
      scope :signed_by, lambda { |signee| where(
        :signee_type   => signee.class.table_name.classify,
        :signee_id     => signee.id)
      }

      scope :signing, lambda { |envelope| where(
        :envelope => envelope
        )
      }
      
      class << self
        def request_signature!(signee, envelope, opts={})
          if !signature_requested_for?(signee, envelope)
            _create_(signee, envelope, opts)
            # update_counter(signee, signees_count: +1)
            # update_counter(envelope, signees_count: +1)
            call_after_create_hooks(signee, envelope.signable)
            # send_esign_notification(:request_signature, { to: opts[:email], subject: opts[:subject], message: opts[:message] }) if !opts[:email].blank?
            true
          else
            false
          end
        end

        def request_signature(signee, envelope, opts={})
          if !signature_requested_for?(signee, envelope)
            _create_(signee, envelope, opts)
            # update_counter(signee, signees_count: +1)
            update_counter(envelope, signees_count: +1)
            call_after_create_hooks(signee, envelope.signable)
            # send_esign_notification(:request_signature, { to: opts[:email], subject: opts[:subject], message: opts[:message] }) if !opts[:email].blank?
            signature_request_for(signee, envelope)
          else
            signature_request_for(signee, envelope)
          end
        end                

        def signature_requested_for?(signee, envelope)
          !signature_request_for(signee, envelope).blank?
        end

        def send_signature_request_email(signee, envelope, subject=nil)
          signature_request = signature_request_for(signee, envelope)
          subject = subject.blank? ? "SignFast Ltd: Signature Request For #{signature_request.signee_name}" : subject
          if !signature_request.signee_email.blank?
            signature_request.send_esign_notification(:request_signature, { to: signature_request.signee_email, subject: subject})
            signature_request.update_attribute('sent_at', Time.now)
            return true
          else
            return false
          end
        end

        def sign!(signee, envelope, opts={})
          # record = signature_request_for(signee, envelope)
          # # record.signed_on = Time.now
          # if opts[:signature_request][:declaration_accepted].to_s == "true"
          #   record.status = "signed"
          #   record.closed_at = Time.now            
          # end
          # if opts[:signature_request][:declaration_accepted].to_s == "false" && !opts[:signature_request][:signee_comment].blank?
          #   record.status = "declined"
          #   record.closed_at = Time.now            
          # end
          # record.assign_attributes(opts[:signature_request])          
          # if record.save!
          #   send_esign_notification(:signature_request_response, { to: opts[:email] }) if !opts[:email].blank?            
          #   return true
          # else
          #   return false            
          # end   
        end

        def unsign!(signee, envelope)
          if signed?(signee, envelope)
            signature_request_for(signee, envelope).destroy!
            # update_counter(signee, signees_count: -1)
            update_counter(envelope, signees_count: -1)
            call_after_destroy_hooks(signee, envelope.signable)
            true
          else
            false
          end
        end

        def signed?(signee, envelope)
          sr = signature_request_for(signee, envelope)
          !sr.blank? && sr.status === "signed"
        end

        # Returns an ActiveRecord::Relation of all the signees of a certain type that are signing envelope
        def signees_relation(envelope, klass, opts = {})
          rel = klass.where(:id =>
            self.select(:signee_id).
              where(:signee_type => klass.table_name.classify).
              where(:envelope => envelope)
          )

          if opts[:pluck]
            rel.pluck(opts[:pluck])
          else
            rel
          end
        end

        # Returns all the signees of a certain type that are signing envelope
        def signees(envelope, klass, opts = {})
          rel = signees_relation(envelope, klass, opts)          
          if rel.is_a?(ActiveRecord::Relation)
            rel.to_a
          else
            rel
          end
        end

        # Returns an ActiveRecord::Relation of all the envelopes of a certain type that are signed by signee
        def envelopes_relation(signee, klass, opts = {})
          rel = klass.where(:id =>
            self.select(:esign_envelope_id).
              where(:envelope => klass).
              where(:signee_type => signee.class.to_s).
              where(:signee_id => signee.id)
          )

          if opts[:pluck]
            rel.pluck(opts[:pluck])
          else
            rel
          end
        end

        # Returns all the envelopes of a certain type that are signed by signee
        def envelopes(signee, klass, opts = {})
          rel = envelopes_relation(signee, klass, opts)
          if rel.is_a?(ActiveRecord::Relation)
            rel.to_a
          else
            rel
          end
        end

        # # Remove all the signees for envelope
        # def remove_signees(envelope)
        #   self.where(:envelope => envelope).destroy_all
        # end

        # Remove all the envelopes for signee
        def remove_envelopes(signee)
          self.where(:signee_type => signee.class.name.classify).
               where(:signee_id => signee.id).destroy_all
        end

        def send_request_signature_email
          SysMailer.request_signature_email(self).deliver_now
          self.update_attribute("sent_at", Time.now)
        end

        def signature_request_for(signee, envelope)
          signed_by(signee).signing(envelope).first
        end

        def decrypt_e_tag(signee, envelope)
          sr = signature_request_for(signee, envelope)
          passphrase = ActiveSupport::KeyGenerator.new(sr.token).generate_key(sr.token, 32)
          cryptor = ActiveSupport::MessageEncryptor.new(passphrase)
          return cryptor.decrypt_and_verify(sr.etag)
        end        
        
        private        
        def _create_(signee, envelope, opts)          
          self.create! do |signature_request|
            signature_request.signee        =   signee
            signature_request.envelope      =   envelope
            signature_request.signee_name   =   opts[:signee_name]
            signature_request.signee_email  =   opts[:signee_email]
            signature_request.status        =   "pending"
            signature_request.token         =   SecureRandom.uuid
            # signature_request.etag          =   encrypt_e_tag(signature_request.token, opts[:signee_email])
            signature_request.signature_request_logs.new(
              subject: "Signature Request created for #{signee.class.name.classify} (Ref: ##{signee.id}) and #{envelope.class.name.classify} (Ref: ##{envelope.id})",
              status: signature_request.status
            )
          end                  
        end

        def encrypt_e_tag(token, signee_email)
          passphrase = ActiveSupport::KeyGenerator.new(token).generate_key(token, 32)
          cryptor = ActiveSupport::MessageEncryptor.new(passphrase)
          return cryptor.encrypt_and_sign(signee_email.to_s.strip)
        end

      end # class << self
      
      def send_esign_notification(notification, *args)
        if Esign.host_url.blank?
          raise Esign::ArgumentError, "No host url specified in config/initializers/esign.rb \r\n uncomment config.host_url and set the value to your application domain url e.g. `http://example.com`"
        end
        if Esign.send_email           
          message = Esign.mailer.send(notification, self, *args)
          # Remove once we move to Rails 4.2+ only.
          if message.respond_to?(:deliver_now)
            message.deliver_now
          else
            message.deliver
          end
        else
          raise Esign::ArgumentError, "Send Email function is currently disabled in config/initializers/esign.rb \r\n Change config.send_email = false \r\n TO config.send_email = true"
        end
      end

    end
  end
end
