module Esign
	module ActiveRecordStores
    class SignatureRequestLog < ActiveRecord::Base
      extend Esign::Stores::Mixins::SignatureRequest

      belongs_to :signature_request
            
      class << self

      end # class << self
    end
end
end
