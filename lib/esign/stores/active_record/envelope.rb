require 'rest_client'
module Esign
  module ActiveRecordStores
    class Envelope < ActiveRecord::Base
      extend Esign::Stores::Mixins::SignatureRequest
      belongs_to :signable, :polymorphic => true
      has_many :signature_requests

      before_save do |record|
        record.content  = CGI.unescapeHTML(CGI.escapeHTML(record.content)) if !record.content.blank?
      end

      class << self
        def _initialize_(signable, signable_content)
          if !envelope_exists_for?(signable)
            _create_(signable, signable_content)
          else
            get_envelope(signable)
          end
        end

        def get_envelope(signable)
          where(signable: signable).first
        end

        def envelope_exists_for?(signable)
          !get_envelope(signable).blank?
        end


        private        
        def _create_(signable, signable_content)          
          self.create! do |envelope|
            envelope.signable = signable
            envelope.ref    = SecureRandom.hex(32)
            envelope.content = signable_content
          end                  
        end

      end # class << self

        def signees
          signature_requests.collect(&:signee)
        end

        def digitally_sign_document(reason, location)          
          begin
            file = get_pdf_copy
            file = File.open(file.path, "rb")
            response =  RestClient.post("https://signfast.co.uk/api/v1/document_signings/", {document_signing: {envelope_id: ref, reason: reason, location: location, unsigned: file}}, { "X-Parse-REST-API-Key" => "#{Esign.signfast_api_key}"})
            File.delete(file)
            return response.body
          rescue RestClient::ExceptionWithResponse => e
            return { success: false, error: e.response.body }.to_json
          rescue Exception => e
            return { success: false, error: e.message }.to_json
          end          
        end

        def fetch_signed_document
          begin
            response = RestClient.get("https://signfast.co.uk/api/v1/document_signings/lookup/#{ref}", { "X-Parse-REST-API-Key" => "#{Esign.signfast_api_key}"})
            return response.body
          rescue RestClient::ExceptionWithResponse => e
            return { success: false, error: e.response.body }.to_json
          rescue Exception => e
            return { success: false, error: e.message }.to_json
          end
        end

        def get_pdf_copy
          kit = PDFKit.new(self.content.to_s.encode('utf-8', 'binary', invalid: :replace, undef: :replace, replace: '').scrub(""),
          :header_center => "Page [page] from [toPage] pages",
          :header_spacing => '5',
          :header_line => true,
          :header_font_name => "Courier new",
          :header_font_size => "14",
          # :footer_center => initial_here_tabs_str,
          :footer_spacing => '5',
          :margin_bottom => '2cm',
          :margin_top    => '2cm',
          :margin_left   => '2cm',
          :margin_right  => '2cm')
          kit.stylesheets << "#{Esign::Engine.root}/app/assets/stylesheets/print_pdf.css"
          # Get an inline PDF
          file = File.open("/tmp/#{"Doc-#{ref.upcase}.pdf"}", "wb")
          file.binmode
          file.write(kit.to_pdf)
          file.close
          return file
        end        
                    
    end
  end
end
