module Esign
  module Stores
    module Mixins
      module SignatureRequest

      public
        def touch(what = nil)
          if what.nil?
            @touch || false
          else
            raise Esign::ArgumentError unless [:all, :signee, :signable, false, nil].include?(what)
            @touch = what
          end
        end

        def after_request_signature(method)
          raise Esign::ArgumentError unless method.is_a?(Symbol) || method.nil?
          @after_create_hook = method
        end 
        
        def after_sign(method)
          raise Esign::ArgumentError unless method.is_a?(Symbol) || method.nil?
          @after_create_hook = method
        end       

        def after_unsign(method)
          raise Esign::ArgumentError unless method.is_a?(Symbol) || method.nil?
          @after_destroy_hook = method
        end

      protected
        def call_after_create_hooks(signee, signable)
          self.send(@after_create_hook, signee, signable) if @after_create_hook
          touch_dependents(signee, signable)
        end

        def call_after_destroy_hooks(signee, signable)
          self.send(@after_destroy_hook, signee, signable) if @after_destroy_hook
          touch_dependents(signee, signable)
        end
      end
    end
  end
end