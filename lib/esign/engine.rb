# frozen_string_literal: true

# module Esign
#   class Engine < Rails::Engine    
#     config.autoload_paths << File.expand_path("../../app/", __dir__)
    
#     initializer "esign.load_app_instance_data" do |app|
#       Esign.setup do |config|
#         config.app_root = app.root
#       end
#     end

#     initializer "esign.load_static_assets" do |app|
#       app.middleware.use ::ActionDispatch::Static, "#{root}/public"
#     end    
#   end
# end