require 'esign'
require "esign/version"
require 'esign/configuration'
require 'simple_form'
require 'rails'
require 'bootstrap-sass'
require 'font-awesome-rails'
require 'active_support/core_ext/numeric/time'
require 'active_support/dependencies'
require 'pdfkit'

%w(
  lib/*.rb
  stores/mixins/base.rb
  stores/mixins/**/*.rb
  stores/active_record/mixins/base.rb
  stores/active_record/mixins/**/*.rb
  stores/redis/mixins/base.rb
  stores/redis/mixins/**/*.rb
  stores/active_record/**/*.rb
  stores/redis/base.rb
  stores/redis/**/*.rb
  actors/**/*.rb
  victims/**/*.rb
  helpers/**/*.rb
  config/**/*.rb
).each do |path|
  Dir["#{File.dirname(__FILE__)}/esign/#{path}"].each { |f| require(f) }
end

ActiveRecord::Base.send :include, Esign::ActsAsHelpers


module Esign
  class << self
    
    # Default way to set up Esign. Run rails generate esign_install to create
    # a fresh initializer with all configuration values.
    def setup
      yield self
    end

  end

  module Mailers
    autoload :Helpers,          'esign/mailers/helpers'
  end

  module Controllers
    autoload :ScopedViews,      'esign/controllers/scoped_views'
  end

  # Devise::Models.send(:autoload, :signature_request, 'esign/models/signature_request')

  mattr_accessor :app_root
  @@app_root = ""

  mattr_accessor :ersd_link
  @@ersd_link = "https://trust.docusign.com/en-us/trust-certifications/data-protection-and-trust-guide/appendix/electronic-record-and-signature-disclosure-capabilities/"

  mattr_accessor :parent_mailer
  @@parent_mailer = "ActionMailer::Base"

  mattr_accessor :mailer_sender
  @@mailer_sender = "test@example.com"

  mattr_accessor :signfast_api_key
  @@signfast_api_key = ""

  class Getter
    def initialize(name)
      @name = name
    end

    def get
      ActiveSupport::Dependencies.constantize(@name)
    end
  end

  def self.ref(arg)
    ActiveSupport::Dependencies.reference(arg)
    Getter.new(arg)
  end
  
# Get the mailer class from the mailer reference object.
  def self.mailer
    @@mailer_ref.get
  end

  # Set the mailer reference object to access the mailer.
  def self.mailer=(class_name)
    @@mailer_ref = ref(class_name)
  end
  self.mailer = "Esign::Mailer"

  mattr_accessor :parent_controller
  @@parent_controller = "ApplicationController"

  mattr_accessor :mailer_reply_to
  @@mailer_reply_to = "no-reply@example.com"

  mattr_accessor :send_email
  @@send_email = true


  
  class Engine < ::Rails::Engine
    isolate_namespace Esign

    initializer 'esign.load_static_assets' do |app|
      app.middleware.unshift ::ActionDispatch::Static, "#{root}/public"
    end

    initializer "esign.view_helpers" do
      # ActionView::Base.send :include, ViewHelpers
    end

    initializer "engine_name.assets.precompile"  do |app|
      app.config.assets.precompile += %w( esign.js esign.scss )
    end    
  
    # set the manifests and assets to be precompiled
    # config.to_prepare do
    #   Rails.application.config.assets.precompile += %w(
    #     esign/*
    #     esign/plugins/*
    #     )
    #   end
    end

  mattr_accessor :engine_url_helpers
  @@engine_url_helpers = Engine.routes.url_helpers

  mattr_accessor :host_url
  @@host_url = ""

  mattr_accessor :host_port
  @@host_port = ""  

end

# Require our engine
# require "esign/engine"