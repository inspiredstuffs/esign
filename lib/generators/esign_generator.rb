require 'rails/generators/base'
# require 'rails/generators/named_base'
require 'rails/generators/migration'


# STORES = %w(active_record redis)
module Esign
  module Generators
  class EsignGenerator < Rails::Generators::Base
    
        include Rails::Generators::Migration

        namespace "esign"
        source_root File.expand_path("../esign/templates", __FILE__)
        class_option :store, :type => :string, :default => 'active_record', :description => "Type of datastore"  
        class_option :routes, desc: "Generate routes", type: :boolean, default: true
        desc "Creates a Esign initializer and copy locale files to your application."
        # class_option :orm      

        def copy_initializer
          # unless options[:orm]
          #   raise MissingORMError, <<-ERROR.strip_heredoc
          #   An ORM must be set to install Esign in your application.
          #   Be sure to have an ORM like Active Record or Redis loaded in your
          #   app or configure your own at `config/application.rb`.
          #     config.generators do |g|
          #       g.orm :your_orm_gem
          #     end
          #   ERROR
          # end

          template "esign.rb", "config/initializers/esign.rb"
        end

        def copy_locale
          copy_file "../../../../config/locales/en.yml", "config/locales/esign.en.yml"
        end

      def self.next_migration_number(dirname)
        if ActiveRecord::Base.timestamped_migrations
          Time.now.utc.strftime("%Y%m%d%H%M%S")
        else
          "%.3d" % (current_migration_number(dirname) + 1)
        end
      end

      def create_migration_file
        # options[:store].downcase!
        # unless STORES.include?(options[:store])
        #   puts "Invalid store '#{options[:store]}'. The following stores are valid: #{STORES.join(', ')}."
        #   exit 1
        # end
        # copy_file "#{options[:store]}/model_esign_envelope.rb",  'app/models/esign_envelope.rb'
        copy_file "#{options[:store]}/model_signature_request.rb",  'app/models/signature_request.rb'
        copy_file "#{options[:store]}/model_signature_request_log.rb",  'app/models/signature_request_log.rb'
        # if options[:store] == 'active_record'
          migration_template "#{options[:store]}/migration_esign_envelope.rb",  'db/migrate/create_esign_envelope.rb'
          sleep 1 # wait a second to have a unique migration timestamp    
          migration_template "#{options[:store]}/migration_esign_signature_requests.rb",  'db/migrate/create_esign_signature_requests.rb'
          sleep 1 # wait a second to have a unique migration timestamp      
          migration_template "#{options[:store]}/migration_esign_signature_request_logs.rb",  'db/migrate/create_esign_signature_request_logs.rb'
          sleep 1 # wait a second to have a unique migration timestamp      
        # end
      end

      def add_esign_routes
        esign_route  = "mount Esign::Engine => '/esign', :as => 'esign'".dup
        # esign_route << %Q(
        #     resources :signature_requests do
        #       collection do
        #         get ':token' => 'signature_requests#index'
        #       end
        #     end
        #   end)
        esign_route << %Q(, skip: :all) unless options.routes?
        route esign_route
      end  

        def show_readme
          # readme "README" if behavior == :invoke
        end

        def rails_4?
          Rails::VERSION::MAJOR == 4
        end 
    end
  end
end