class CreateEsignEnvelope < ActiveRecord::Migration<%= "[#{Rails::VERSION::MAJOR}.#{Rails::VERSION::MINOR}]" if Rails.version.start_with? '5'  %>
  def change
    create_table    :esign_envelopes do |t|
      t.string      :ref, index: true, unique: true
      t.integer     :signable_id
      t.string      :signable_type
      t.text 		:content, limit: 16.megabytes - 1
      t.timestamps
    end

    add_index :esign_envelopes, ["signable_id", "signable_type"], :name => "esign_signables"
  end
end
