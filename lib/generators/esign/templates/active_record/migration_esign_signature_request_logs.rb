class CreateEsignSignatureRequestLogs < ActiveRecord::Migration<%= "[#{Rails::VERSION::MAJOR}.#{Rails::VERSION::MINOR}]" if Rails.version.start_with? '5'  %>
  def change
    create_table    :esign_signature_request_logs do |t|
      t.integer     :signature_request_id, index: true
      t.string      :status
      t.string      :subject
      t.text        :body      
      t.timestamps
    end
  end
end
