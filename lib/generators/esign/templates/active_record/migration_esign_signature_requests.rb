class CreateEsignSignatureRequests < ActiveRecord::Migration<%= "[#{Rails::VERSION::MAJOR}.#{Rails::VERSION::MINOR}]" if Rails.version.start_with? '5'  %>
  def change
    create_table    :esign_signature_requests do |t|
      t.integer     :envelope_id, index: true
      t.integer     :signee_id
      t.string      :signee_type
      t.string      :token, index: true, unique: true
      t.string      :status
      t.datetime    :sent_at
      t.datetime    :closed_at
      t.boolean     :declaration_accepted,  default: false
      t.string      :signee_name
      t.string      :signee_email
      t.text        :signee_comment
      t.string      :signee_ip
      t.string      :signee_user_agent
      t.decimal     :signee_latitude, precision: 10, scale: 6
      t.decimal     :signee_longitude, precision: 10, scale: 6
      t.timestamps
    end

    add_index :esign_signature_requests, ["signee_id", "signee_type"],     :name => "esign_signees"      
  end
end
