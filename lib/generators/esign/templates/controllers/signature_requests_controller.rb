# frozen_string_literal: true

class <%= @scope_prefix %>SignatureRequestsController < Esign::SignatureRequestsController
  before_action :find_signature_request, only: [:index]
  # GET /resource/signature_requests?token=abcdef
  def index
    super
  end
  # PUT /resource/signature_requests/sign/:id
  def sign
    super
  end 

  protected
  def find_signature_request
    super
  end 
end