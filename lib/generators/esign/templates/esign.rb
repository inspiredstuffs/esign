Esign.setup do |config|
  # The secret key used by Esign. Esign uses this key to generate
  # random tokens. Changing this key will render invalid all existing
  # confirmation, reset password and unlock tokens in the database.
  # Esign will use the `secret_key_base` as its `secret_key`
  # by default. You can change it below and use your own secret key.
  # config.secret_key = '<%= SecureRandom.hex(64) %>'

  # ==> Mailer Configuration
  # Configure the e-mail address which will be shown in Esign::Mailer,
  # note that it will be overwritten if you use your own mailer class
  # with default "from" parameter.
  config.mailer_sender = 'please-change-me-at-config-initializers-esign@example.com'
    # Configure the class responsible to send e-mails.
  # config.mailer = 'Esign::Mailer'
  config.mailer_reply_to = 'please-change-me-at-config-initializers-esign@example.com'

  # Configure the parent class responsible to send e-mails.
  # config.parent_mailer = 'ActionMailer::Base'

  # ==> ORM configuration
  # Load and configure the ORM. Supports :active_record (default) and
  # :mongoid (bson_ext recommended) by default. Other ORMs may be
  # available as additional gems.
  # require 'esign/orm/<%#= options[:orm] %>'  

  # Configure the Amazon S3 bucket credentials for storing captured signature.  

  # Configure the link to “ELECTRONIC RECORD AND SIGNATURE DISCLOSURE” CAPABILITIES
  # config.ersd_link = ""

  # Configure Esign to send emails or create your own customised emails. Default is true
  # config.send_email = true

  #config.host_url = 'esign.example.com'
  #config.host_port = '80'

  #Your app needs to be registered on https://signfast.co.uk before you can sign document digitally
  #config.signfast_api_key = '323hklsj-9494jmjs-232skdl-232322-2344'

end